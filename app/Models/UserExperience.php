<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserExperience extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $fillable = [
        'experience',
        'user_detail_id'
    ];
    
    public function UserDetail()
    {
        return $this->belongsTo(UserDetail::class);
    }
}
