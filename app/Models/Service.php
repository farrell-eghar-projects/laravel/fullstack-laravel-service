<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $fillable = [
        'title',
        'description',
        'delivery_time',
        'revision_limit',
        'price',
        'note',
        'user_id'
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    
    public function userAdvantages()
    {
        return $this->hasMany(UserAdvantage::class);
    }
    
    public function serviceAdvantages()
    {
        return $this->hasMany(ServiceAdvantage::class);
    }
    
    public function serviceThumbnails()
    {
        return $this->hasMany(ServiceThumbnail::class);
    }
    
    public function taglines()
    {
        return $this->hasMany(Tagline::class);
    }
}
