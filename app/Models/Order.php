<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $fillable = [
        'file',
        'note',
        'buyer_id',
        'freelancer_id',
        'service_id',
        'order_status_id',
        'expired_at'
    ];
    
    public function userBuyer()
    {
        return $this->belongsTo(User::class, 'buyer_id');
    }
    
    public function userFreelancer()
    {
        return $this->belongsTo(User::class, 'freelancer_id');
    }
    
    public function service()
    {
        return $this->belongsTo(Service::class);
    }
    
    public function orderStatus()
    {
        return $this->belongsTo(OrderStatus::class);
    }
}
