<?php

namespace Database\Seeders;

use App\Models\UserDetail;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userDetails = [
            [
                'user_id'           => 2,
                'photo'             => '',
                'role'              => 'Website Developer',
                'contact_number'    => '',
                'biography'         => '',
                'created_at'        => date('Y-m-d h:i:s'),
                'updated_at'        => date('Y-m-d h:i:s')
            ],
            [
                'user_id'           => 3,
                'photo'             => '',
                'role'              => 'UI/UX Designer',
                'contact_number'    => '',
                'biography'         => '',
                'created_at'        => date('Y-m-d h:i:s'),
                'updated_at'        => date('Y-m-d h:i:s')
            ]
        ];
        
        UserDetail::insert($userDetails);
    }
}
