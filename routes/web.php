<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Landing\LandingController;
use App\Http\Controllers\Dashboard\MemberController;
use App\Http\Controllers\Dashboard\MyOrderController;
use App\Http\Controllers\Dashboard\ProfileController;
use App\Http\Controllers\Dashboard\RequestController;
use App\Http\Controllers\Dashboard\ServiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', LandingController::class)->only(['index']);

Route::group([
    'as'      => 'landing.',
    'controller'=> LandingController::class
], function() {
    Route::get('detail-booking/{id}', 'bookingDetail')->name('booking.detail');
    Route::get('booking/{id}', 'booking')->name('booking');
    Route::get('detail/{id}', 'detail')->name('detail');
    Route::get('explore', 'explore')->name('explore');
});

Route::group([
    'as'        => 'member.',
    'prefix'    => 'member',
    'middleware'=> ['auth:sanctum', 'verified']
], function() {
    Route::resource('dashboard', MemberController::class);
    Route::resource('service', ServiceController::class);
    
    Route::get('approve-request/{id}', [RequestController::class, 'approve'])->name('request.approve');
    Route::resource('request', RequestController::class);
    
    Route::get('order/{id}/accept', [MyOrderController::class, 'accept'])->name('order.accept');
    Route::get('order/{id}/reject', [MyOrderController::class, 'reject'])->name('order.reject');
    Route::resource('order', MyOrderController::class);
    
    Route::get('delete-photo', [ProfileController::class, 'deletePhoto'])->name('profile.delete.photo');
    Route::resource('profile', ProfileController::class);
});

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::middleware([
//     'auth:sanctum',
//     config('jetstream.auth_session'),
//     'verified'
// ])->group(function () {
//     Route::get('/dashboard', function () {
//         return view('dashboard');
//     })->name('dashboard');
// });
